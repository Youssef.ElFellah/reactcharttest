import MyBarChart from "./Components/Charts/MyChart";
import { useReadFils } from "./Hooks/ReadFile";

function App() {
  const fileData = useReadFils({ path: "src/assets/files/random_data.csv" });
  for (let i = 0; i < 5; i++) {
    console.log(fileData[i]);
  }

  return <div>{<MyBarChart data={fileData} />}</div>;
}

export default App;
