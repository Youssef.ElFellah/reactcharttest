import { useMemo, useState } from "react";
import { BarChart, Bar, XAxis, YAxis, Tooltip } from "recharts";
import Select from "react-select";
import _, { keyBy } from "lodash";
import { AggragateData, FilterData } from "../../Utils/DataManipulation";

type MyBarChartProps = {
  data: Array<any>;
};

const CustomizedBar = ({ x, y, width, height, value }: any) => {
  return (
    <rect
      x={x}
      y={y}
      width={width}
      height={height}
      fill={value > 100 ? "#82ca9d" : "#8884d8"}
    />
  );
};
function MyBarChart({ data }: MyBarChartProps) {
  const [filter, setFilter] = useState(null);
  const [groupBy, setGroupBy] = useState<string>("code");

  const filteredData = FilterData(data, filter);

  const options = data.map((item) => ({ value: item.id, label: item.id }));
  data.forEach((item) => {
    options.push({ value: item.code, label: item.code });
  });
  options.unshift({ value: null, label: "reset" });

  const aggragatedData = AggragateData(groupBy, filteredData);

  const chartRender = useMemo(() => {
    const transformedDictionary = Object.entries(aggragatedData).map(
      ([key, values]) => {
        return { key, values };
      }
    );

    if (groupBy === "month") {
      return transformedDictionary?.map((data: any) => {
        console.log(data);
        return (
          <BarChart width={1700} height={400} data={data.values}>
            <XAxis dataKey="date" />
            <YAxis />
            <YAxis />
            <Tooltip />
            <Bar dataKey="value" shape={<CustomizedBar />} />
          </BarChart>
        );
      });
    }
    if (groupBy === "year") {
      return transformedDictionary?.map((data: any) => {
        return (
          <BarChart width={1700} height={400} data={data.values}>
            <XAxis dataKey="date" />
            <YAxis />
            <YAxis />
            <Tooltip />
            <Bar dataKey="value" shape={<CustomizedBar />} />
          </BarChart>
        );
      });
    }
    if (groupBy === "code") {
      return (
        <BarChart width={1700} height={500} data={aggragatedData}>
          <XAxis dataKey="date" />
          <YAxis />
          <YAxis />
          <Tooltip />
          <Bar dataKey="value" shape={<CustomizedBar />} />
        </BarChart>
      );
    }
  }, [groupBy, aggragatedData]);

  return (
    <>
      <Select
        options={options}
        onChange={({ value }: any) => setFilter(value)}
      />
      <div>
        <label>
          <input
            type="radio"
            value="code"
            checked={groupBy === "code"}
            onChange={(e) => setGroupBy(e.target.value)}
          />
          Code
        </label>
        <label>
          <input
            type="radio"
            value="month"
            checked={groupBy === "month"}
            onChange={(e) => setGroupBy(e.target.value)}
          />
          Month
        </label>
        <label>
          <input
            type="radio"
            value="year"
            checked={groupBy === "year"}
            onChange={(e) => setGroupBy(e.target.value)}
          />
          Year
        </label>
      </div>
      {chartRender}
    </>
  );
}

export default MyBarChart;
