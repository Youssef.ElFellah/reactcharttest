import Papa from "papaparse";
import { useState, useEffect } from "react";

type UseReadFilsProps = {
  path: string;
};

export const useReadFils = (props: UseReadFilsProps) => {
  const { path } = props;

  const [data, setData] = useState<Array<unknown>>([]);

  useEffect(() => {
    fetch(path)
      .then((response) => response.text())
      .then((data) => {
        const parsedData = Papa.parse(data, {
          header: true,
        });
        setData(parsedData.data);
      });
  }, []);

  return data;
};
