import _ from "lodash";

export const AggragateData = (groupBy: string, data: any) => {
  let processedData;

  if (groupBy === "code") {
    processedData = _.groupBy(data, "code");
    processedData = Object.entries(processedData).map(([key, value]) => {
      return { code: key, value: _.sumBy(value, "value") };
    });
  } else if (groupBy === "month") {
    processedData = _.groupBy(data, (item) => {
      return new Date(item.date).getMonth();
    });
  } else if (groupBy === "year") {
    processedData = _.groupBy(data, (item) => {
      return new Date(item.date).getFullYear();
    });
  }
  return processedData;
};

export const FilterData = (data: any, filter: any) => {
  return data.filter(
    (item: any) => !filter || item.id === filter || item.code === filter
  );
};
